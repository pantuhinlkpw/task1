#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include "itoa.h"
#define MaxName 16
#define MaxMsg 256
#define ServName "|Server|"
#define IP_Serv "0.0.0.0"

struct socklist_t {
	int elem; //номер дескриптора сокета клиента
	char name[MaxName]; //имя клиента
	char msg[MaxMsg]; //сообщение клиента
	struct socklist_t *next;
};
typedef struct socklist_t *SL;

void Serv(int port); //главная функция, выполняющая работу сервера
//работа со списком сокетов
SL addlist(SL socklist, int sock, const char *name); //добавляет сокет в список
SL remlist(SL socklist, int sock); //исключает сокет из списка
void outlist(SL socklist, int fd); //выводит список в поток fd
//вспомогательные функции
int checkmsg(char *str); //проверяет наличие \n или \r в строке
SL commands(int fd, SL socklist, char *cmd); //анализирует команду, присланную клиентом
void sendall(int sd, SL socklist, char *msg, char *name); //рассылает msg всем клиентам из socklist
SL stopserv(SL socklist); //разрывает соединение со всеми клиентами в socklist и очищает его
void handler1(int sig); //посылает сыну SIGINT
void handler2(int sig); //обработчик для сигнала SIGINT, поднимает флаг завершения сервера
int checknick(SL socklist, char *nick); //проверяет свободен ли данный ник
int checkcmd(char *msg, const char *cmd); //убирает \n и \t из msg и возвращает результат сравнения msg и cmd
void help(int fd); //отправляет список доступных команд пользователю

int flag = 0; //флаг для завершения сервера
int pid; //pid сына

int main(int argc, char **argv) {
	if (argc <= 1) {
		printf("Port isn't entered\n");
		return 1;
	}
	int port = atoi(argv[1]);
	if (0 == (pid = fork())) {
		//сын;
		signal(SIGINT, handler2);
		Serv(port);
		exit(1);
	} else {
		//отец
		signal(SIGINT, handler1);
		int res, stat;
		char buf[MaxName];
		memset(buf, '\0', MaxName);
		for(;;) {
			fd_set readfds;
			FD_ZERO(&readfds);
			FD_SET(0, &readfds);
			res = select(1, &readfds, NULL, NULL, NULL);
			if (res < 1) {
				if (errno != EINTR) {
					//ошибка в select
					perror("select");
					continue;
				} else {
					//проверка на SIGCHLD
					if (0 != waitpid(pid, &stat, WNOHANG)) {
						//процесс-сын завершился по ошибке
						return 1;
					}
				}
			}
			//завершение по сигналу
			if (flag) {
				wait(&stat);
				if (WIFEXITED(stat))
					return  WEXITSTATUS(stat);
				else
					return 1;
			}
			//пришёл не сигнал
			read(0, buf, MaxName);
			if (checkmsg(buf)) {
				if (!strcmp(buf, "shutdown\n")) {
					kill(pid, SIGINT);
					wait(&stat);
					if (WIFEXITED(stat))
						return  WEXITSTATUS(stat);
					else
						return 1;
				}
				else {
					write(1, "Wrong command\n", 15);
					memset(buf, '\0', MaxName);
				}
			} else {
				write(1, "Very long command\n", 19);
				memset(buf, '\0', MaxName);
			}
		}
	}
}

void Serv(int port) {
	char name[MaxName], buf[MaxMsg];
	int ns, res, fd;
	int ls;
	SL socklist = NULL;
	SL temp;
	write(1, "Server is starting...\n", 23);
	ls = socket(AF_INET, SOCK_STREAM, 0);
	if (-1 == ls) {
		//обработка ошибок
		perror("socket");
		exit(1);
	}
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	/*if (0 == inet_aton(IP_Serv, &(addr.sin_addr))) {
		//обработка ошибки
		perror("inet_aton");
		close(ls);
		exit(1);
	}*/
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	int opt = 1;
	setsockopt(ls, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)); //предотвращение залипания порта
	if (0 != bind(ls, (struct sockaddr *) &addr, sizeof(addr))) {
		//обработка ошибок
		perror("bind");
		close(ls);
		exit(1);
	}
	if (-1 == listen(ls, 5)) {
		//обработка ошибок
		perror("listen");
		close(ls);
		exit(1);
	}
	write(1, "Server has started\n", 20);
	for (;;) {
		fd_set readfds;
		int max_d = ls;
		FD_ZERO(&readfds);
		FD_SET(ls, &readfds);
		//просмотр списка клиентов и добавление их во множество readfds
		for (temp = socklist; temp != NULL; temp = temp->next) {
			fd = temp->elem;
			FD_SET(fd, &readfds);
			if (fd > max_d)
				max_d = fd;
		}
		res = select(max_d + 1, &readfds, NULL, NULL, NULL);
		if (res < 1) {
			if (errno != EINTR) {
				//ошибка в select
				perror("select");
				continue;
			} else if (flag) {
				//завершение работы сервера
				socklist = stopserv(socklist);
				close(ls);
				exit(0);
			}
		}
		if (FD_ISSET(ls, &readfds)) {
			//запрос на соединение
			ns = accept(ls, NULL, NULL);
			if (ns == -1) {
				//обработка ошибок
				perror("accept");
				continue;
			}
			//создание никнейма по умолчанию
			memset(name, '\0', MaxName);
			itoa(ns, name);
			strcat(name, "resu");
			reverse(name);
			//добавление нового пользователя в список
			socklist = addlist(socklist, ns, name);
			//подготовка и отправка информационного сообщения
			memset(buf, '\0', MaxMsg);
			strcpy(buf, name);
			strcat(buf, " has logged in\n");
			sendall(0, socklist, buf, ServName);
			memset(buf, '\0', MaxMsg);
		}
		//просмотр списка клиентов
		for (temp = socklist; temp != NULL; temp = temp->next) {
			fd = temp->elem;
			if (FD_ISSET(fd, &readfds)) {
				//пришли данные от клиента
				if (0 == recv(fd, buf, MaxMsg, 0)) {
					//конец файла
					shutdown(fd, SHUT_RDWR);
					close(fd);
					memset(buf, '\0', MaxMsg);
					strcpy(buf, temp->name);
					socklist = remlist(socklist, fd);
					strcat(buf, " has logged out\n");
					sendall(fd, socklist, buf, ServName);
					memset(buf, '\0', MaxMsg);
				}
				else {
					//непустые данные
					if ((strlen(temp->msg) + strlen(buf)) >= MaxMsg) {
						send(fd, "-----\n", 7, 0);
						send(fd, "Very long message\n", 19, 0);
						send(fd, "-----\n", 7, 0);
						memset(temp->msg, '\0', MaxMsg);
					} else
						strcat(temp->msg, buf);
					memset(buf, '\0', MaxMsg);
					if (checkmsg(temp->msg)) {
						if ('/' == temp->msg[0])
							socklist = commands(fd, socklist, temp->msg);
						else
							sendall(fd, socklist, temp->msg, temp->name);
						memset(temp->msg, '\0', MaxMsg);
					}
				}
			}
		}
	}
}

SL addlist(SL socklist, int sock, const char *name) {
	if (socklist == NULL) {
		socklist = malloc(sizeof(struct socklist_t));
		socklist->elem = sock;
		strcpy(socklist->name, name);
		socklist->next = NULL;
	}
	else {
		SL temp = socklist;
		while (temp->next != NULL) 
			temp = temp->next;
		temp->next = malloc(sizeof(struct socklist_t));
		temp = temp->next;
		temp->elem = sock;
		strcpy(temp->name, name);
		temp->next = NULL;
	}
	return socklist;
}

SL remlist(SL socklist, int sock) {
	if (socklist == NULL) return socklist;
	SL temp = socklist;
	if (temp->elem == sock) {
		socklist = socklist->next;
		free(temp);
		return socklist;
	}
	for (; temp->next != NULL; temp = temp->next)
		if (temp->next->elem == sock) {
			SL t = temp->next;
			temp->next = temp->next->next;
			free(t);
			break;
		}
	return socklist;
}

void outlist(SL socklist, int fd) {
	write(fd, "-----\n", 7);
	write(fd, "Users online:\n", 14);
	for (; socklist != NULL; socklist = socklist->next)
		write(fd,  socklist->name, MaxName);
	write(fd, "\n-----\n", 8);
	return;	 
}

int checkmsg(char *str) {
	int i;
	for (i = 0; str[i] != '\0'; i++) {
		if ((str[i] == '\n') || (str[i] == '\t'))
			return 1;
	}
	return 0;
}

SL commands(int fd, SL socklist, char *cmd) {
	int i, j;
	SL temp;
	static char msg[MaxMsg];
	memset(msg, '\0', MaxMsg);
	static char name[MaxName];
	memset(name, '\0', MaxName);
	switch (cmd[1]) {
		case 'n':
			//проверка команды на корректность
			if (checkcmd(cmd, "/nick")) {
				send(fd, "-----\n", 7, 0);
				send(fd, "Wrong command\nPossible commands:\n", 34, 0);
				help(fd);
				send(fd, "-----\n", 7, 0);
				return socklist;
			}
			//поиск нужного клиента
			for (temp = socklist; temp->elem != fd; temp = temp->next)
				if (temp->next == NULL) {
					//обработка ошибки: отсутствует клиент с таким дескриптором
					send(fd, "-----\n", 7, 0);
					send(fd, "Error. Nick hasn't changed\n", 28, 0);
					write(2, "Commands: user doesn't exist\n", 30);
					send(fd, "-----\n", 7, 0);
					return socklist;
				}	
			//копирование аргумента в поле name
			strcpy(msg, temp->name);
			for(j = 0, i = 6; (cmd[i] != '\0') && (j < MaxName); i++, j++) {
				if ((cmd[i] == ' ') || (cmd[i] == '\t')) {
					--j;
					continue;
				}
				name[j] = cmd[i];
			}
			if (j == 0) {
				send(fd, "-----\n", 7, 0);
				send(fd, "Argument doesn't exist\n", 23, 0);
				send(fd, "-----\n", 7, 0);
				return socklist;
			} else if (j >= MaxName) {
				send(fd, "-----\n", 7, 0);
				send(fd, "Very long nick\n", 16, 0);
				send(fd, "-----\n", 7, 0);
				return socklist;
			}
			name[j] = '\0';
			//проверка свободен ли ник
			if (checknick(socklist, name)) {
				send(fd, "-----\n", 7, 0);
				send(fd, "This nick is used\n", 19, 0);
				send(fd, "-----\n", 7, 0);
				return socklist;
			}
			memset(temp->name, '\0', MaxName);
			strcpy(temp->name, name);
			//подготовка и отправка информационного сообщения
			strcat(msg, " has changed name to ");
			strcat(msg, temp->name);
			strcat(msg, "\n");
			sendall(0, socklist, msg, ServName);
			memset(msg, '\0', MaxMsg);
			break;
		case 'u':
			//проверка команды на корректность
			if (checkcmd(cmd, "/users")) {
				send(fd, "-----\n", 7, 0);
				send(fd, "Wrong command\nPossible commands:\n", 34, 0);
				help(fd);
				send(fd, "-----\n", 7, 0);
				return socklist;
			}
			//отправка информации клиенту
			outlist(socklist, fd);
			break;
		case 'e':
			//проверка команды на корректность
			if (checkcmd(cmd, "/exit")) {
				send(fd, "-----\n", 7, 0);
				send(fd, "Wrong command\nPossible commands:\n", 34, 0);
				help(fd);
				send(fd, "-----\n", 7, 0);
				return socklist;
			}
			//закрытие соединения
			shutdown(fd, SHUT_RDWR);
			close(fd);
			//поиск имени пользователя
			for (temp = socklist; temp->elem != fd; temp = temp->next)
				if (temp->next == NULL) {
					send(fd, "-----\n", 7, 0);
					send(fd, "Error. Try again\n", 28, 0);
					write(2, "Commands: user doesn't exist\n", 30);
					send(fd, "-----\n", 7, 0);
				}
			//подготовка и отправка информационного сообщения
			memset(msg, '\0', MaxMsg);
			strcpy(msg, temp->name);
			socklist = remlist(socklist, fd);
			strcat(msg, " has logged out\n");
			sendall(0, socklist, msg, ServName);
			memset(msg, '\0', MaxMsg);
			break;
		case 'h':
			//проверка команды на корректность
			if (checkcmd(cmd, "/help")) {
				send(fd, "-----\n", 7, 0);
				send(fd, "Wrong command\nPossible commands:\n", 34, 0);
				help(fd);
				send(fd, "-----\n", 7, 0);
				return socklist;
			}
			//отправка информации клиенту
			help(fd);
			break;
		default: 
			send(fd, "-----\n", 7, 0);
			send(fd, "Wrong command\nPossible commands:\n", 34, 0);
			help(fd);
			send(fd, "-----\n", 7, 0);
			break;
	}
	return socklist;
}

void sendall(int sd, SL socklist, char *msg, char *name) {
	int fd = 1, lenmsg = strlen(msg) + strlen(name) + 3;
	char *p = malloc(lenmsg);
	strcpy(p, name);
	strcat(p, ": ");
	strcat(p, msg);
	write(fd, p, lenmsg);
	if (socklist == NULL) {
		free(p);
		return;
	}
	SL tmp;
	for (tmp = socklist; tmp != NULL; tmp = tmp->next) {
		fd = tmp->elem;
		if (fd == sd) continue;
		send(fd, p, lenmsg, 0);
	}
	free(p);
	return;
}

SL stopserv(SL socklist) {
	sendall(0, socklist, "Server is shutting down\nGood bye!\n", ServName);
	SL temp;
	while (socklist != NULL) {
		shutdown(socklist->elem, SHUT_RDWR);
		close(socklist->elem);
		temp = socklist;
		socklist = socklist->next;
		free(temp);
	}
	return socklist;
}

void handler1(int sig) {
	flag = 1;
	kill(pid, SIGINT);
	return;
}

void handler2(int sig) {
	flag = 1;
	return;
}

int checknick(SL socklist, char *nick) {
	SL tmp = socklist;
	for(; tmp != NULL; tmp = tmp->next) {
		if (!strcmp(tmp->name, nick)) return 1;
	}
	if (!strcmp(ServName, nick)) return 1;
	return 0;
}

int checkcmd(char *msg, const char *cmd) {
	int i, j;
	char *p = malloc(strlen(msg) + 1);
	for (j = 0, i = 0; msg[i] != '\0'; ++i, ++j) {
		if ((msg[i] == '\n') || (msg[i] == '\r')) {
			--j;
			continue;
		}
		p[j] = msg[i]; 
	}
	p[j] = '\0';
	strcpy(msg, p);
	i = 0;
	for (j = 0; p[j] != '\0'; ++j) {
		if (p[j] == ' ') i = 1;
		if (i) p[j] = '\0';
	}
	i = strcmp(p, cmd);
	free(p);
	return i;
}

void help(int fd) {
	send(fd, " /nick <new name> - change nickname\n", 37, 0);
	send(fd, " /users - information about online users\n", 42, 0);
	send(fd, " /exit - exit from chat\n", 25, 0);
	send(fd, " /help - possible commands list\n", 33, 0);
	return;
}

